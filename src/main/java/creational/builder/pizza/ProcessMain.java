package creational.builder.pizza;

public class ProcessMain {
    public static void main(String[] args) {
	
	Directeur directeur = new Directeur();
	
	MonteurPizza monteurPizzaHawaii  = new MonteurPizzaHawaii();
	MonteurPizza monteurPizzaPiquante = new MonteurPizzaPiquante();

	directeur.setMonteurPizza(monteurPizzaHawaii);
	/* Building PizzaHawaii */
	directeur.construirePizza();
	Pizza pizzas = directeur.getPizza();
	/* Building PizzaHawaii */
	directeur.setMonteurPizza(monteurPizzaPiquante);
	directeur.construirePizza();
	pizzas = directeur.getPizza();

    }
}