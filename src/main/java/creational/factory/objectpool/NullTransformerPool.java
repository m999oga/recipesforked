package creational.factory.objectpool;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;

import org.apache.log4j.Logger;




public class NullTransformerPool extends ObjectPool
{
    private static Logger log =
        Logger.getLogger(NullTransformerPool.class);

  /***** INITIALISATION *****/

  public NullTransformerPool() {
    super(25, 35);
    super.fillPool();   // required by super class.
  }

  /***** HOT SPOT *****/
  protected Object createPooledInstance()
  {
  	
	Transformer trans = null;
	try {
		TransformerFactory transFact = TransformerFactory.newInstance();
		trans = transFact.newTransformer();
		
	} catch (TransformerConfigurationException ex) {
		log.fatal("Error creating a transformer", ex);
		return null;
	}

	trans.setOutputProperty(OutputKeys.INDENT, "yes");
	trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
	return trans;
  	
  }
  // Convenient method - casts returned object to Transformer.
  public Transformer getNullTransformer() {
    return (Transformer)super.getInstance();
  }

  /***** STATICS *****/

  private static NullTransformerPool singleInstance = null;
  public synchronized static NullTransformerPool getDefault() {
    if (singleInstance == null) {
      singleInstance = new NullTransformerPool();
    }
    return singleInstance;
  }

}
