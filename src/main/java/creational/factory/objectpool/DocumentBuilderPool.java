package creational.factory.objectpool;



import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;


public class DocumentBuilderPool extends ObjectPool
{
    private static Logger log =
        Logger.getLogger(DocumentBuilderPool.class);
/***** INITIALISATION *****/

  private DocumentBuilderFactoryPool dbfPool = null;

  public DocumentBuilderPool() {
    super(25, 35);
    dbfPool = DocumentBuilderFactoryPool.getDefault();
    super.fillPool();   // requirement from SuperClass.
  }

  /***** HOT SPOT *****/
  protected Object createPooledInstance()
  {
    DocumentBuilder db = null;
    DocumentBuilderFactory dbf = (DocumentBuilderFactory) dbfPool.getInstance();
    try {
        db = dbf.newDocumentBuilder();
    } catch (ParserConfigurationException ex) {
        log.fatal("Error retrieving the documentBuilder", ex);
    }
    dbfPool.releaseInstance(dbf);
    return db;
  }

  // Convenient method - casts returned object to DocumentBuilder.
  public DocumentBuilder getDocumentBuilder() {
    return (DocumentBuilder)super.getInstance();
  }

  /***** STATICS *****/

  private static DocumentBuilderPool singleInstance = null;
  public synchronized static DocumentBuilderPool getDefault() {
    if (singleInstance == null) {
      singleInstance = new DocumentBuilderPool();
    }
    return singleInstance;
  }

}

