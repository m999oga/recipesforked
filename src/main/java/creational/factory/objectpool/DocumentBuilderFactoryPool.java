package creational.factory.objectpool;


import javax.xml.parsers.DocumentBuilderFactory;

public class DocumentBuilderFactoryPool extends ObjectPool
{
  /***** INITIALISATION *****/

  /**
   * Pool for DocumentBuilderFactory instances. By default, these
   * are non-validating and namespaceAware.
   */
  public DocumentBuilderFactoryPool() {
    super(25, 35);
    super.fillPool(); // requirement from super class.
  }

  /***** HOT SPOT *****/

  protected Object createPooledInstance()
  {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setValidating(false);
    dbf.setNamespaceAware(true);
    return dbf;
  }

  public DocumentBuilderFactory getDocBuilderFactory() {
    return (DocumentBuilderFactory) super.getInstance();
  }

  /***** STATICS *****/

  private static DocumentBuilderFactoryPool singleInstance = null;
  public synchronized static DocumentBuilderFactoryPool getDefault() {
    if (singleInstance == null) {
      singleInstance = new DocumentBuilderFactoryPool();
    }
    return singleInstance;
  }

}
