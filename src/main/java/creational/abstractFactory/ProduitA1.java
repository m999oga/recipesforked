package creational.abstractFactory;

public class ProduitA1 extends AbstractProductA {
    @Override
    public void methodeA() {
	System.out.println("ProduitA1.methodeA()");
    }

}
