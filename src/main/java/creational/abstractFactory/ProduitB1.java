package creational.abstractFactory;

public class ProduitB1 extends AbstractProductB {

    @Override
    public void methodeB() {
	System.out.println("ProduitB1.methodeB()");
    }

}
