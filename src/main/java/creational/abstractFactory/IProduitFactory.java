package creational.abstractFactory;

public interface IProduitFactory {
    public AbstractProductA getProduitA();
    public AbstractProductB getProduitB();
}
