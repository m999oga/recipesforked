package creational.abstractFactory;

public class ProcessMain {
    public static void main(String[] args) {
	
	IProduitFactory produitFactory1 = new ProduitFactory1();
	IProduitFactory produitFactory2 = new ProduitFactory2();
	
	AbstractProductA produitA = produitFactory1.getProduitA();
	AbstractProductB produitB = produitFactory1.getProduitB();
	
	System.out.println("Utilisation de la premiere fabrique");
	
	produitA.methodeA();
	produitB.methodeB();
	
	System.out.println("Utilisation de la seconde fabrique");
	
	produitA = produitFactory2.getProduitA();
	produitB = produitFactory2.getProduitB();
	
	produitA.methodeA();
	produitB.methodeB();
    }
}
