package creational.singleton;

/*
 * en environnement multithreadé, deux threads peuvent exécuter 
 * l'accès simultanément et créer ainsi chacun une instance du singleton. 
 * Elle doit donc être absolument proscrite.
 * Afin de résoudre ce problème de concurrence des threads, 
 * on peut évidemment synchroniser la méthode "getInstance()" 
 */
public class SingletonGlobal
{
	/** Constructeur privé */
	private SingletonGlobal()
	{}
 
	/** Instance unique non préinitialisée */
	private static SingletonGlobal INSTANCE = null;
 
	/** Point d'accès pour l'instance unique du singleton */
	public static synchronized SingletonGlobal getInstance()
	{			
		if (INSTANCE == null)
		{ 	INSTANCE = new SingletonGlobal();	
		}
		return INSTANCE;
	}
}