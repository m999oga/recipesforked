package creational.singleton;
/*
 * Cette technique joue sur le fait que la classe interne ne sera chargée 
 * en mémoire que lorsque l'on y fera référence pour la première fois, 
 * c'est-à-dire lors du premier appel de getInstance() sur la classe Singleton.
 * Lors de son chargement, le Holder initialisera ses champs statiques 
 * et créera donc l'instance unique du Singleton.
 * 
 * Cerise sur le gâteau, elle fonctionne correctement en environnement 
 * multithreadé et ne nécessite aucune synchronisation explicite !
 */

public class Singleton {
    /**
    * La présence d'un constructeur privé supprime
    * le constructeur public par défaut.
    */
    private Singleton() {
    }
    /**
    * SingletonHolder est chargé à la première exécution de
    * Singleton.getInstance() ou au premier accès à SingletonHolder.instance ,
    * pas avant.
    */
    private static class SingletonHolder {
	private final static Singleton instance = new Singleton();
    }
    /**
    * Méthode permettant d'obtenir l'instance unique.
    * @return L'instance du singleton.
    */
    public static Singleton getInstance() {
	return SingletonHolder.instance;
    }
}
