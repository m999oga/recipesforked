package behavioral.visitor.rulesengine;

public interface NumberElement {
    public void accept(NumberVisitor visitor);
}