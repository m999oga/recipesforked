package behavioral.visitor.exemple;

/**
* Définit l'interface d'un élément
*/
public interface Element {
    public void recevoirVisiteur(Visiteur pVisiteur);
}
