package behavioral.template.exemple;

/**
* Sous-classe concrète de AbstractClasse
* Implémente les méthodes utilisées par l'algorithme
* de la méthode operationTemplate() de AbstractClasse
*/
public class ConcreteClasse extends AbstractClasse {
    public void operationAbs1() {
	System.out.println("operationAbs1");
    }
    public void operationAbs2(int pNombre) {
	System.out.println("\toperationAbs2 : " +pNombre);
    }
}