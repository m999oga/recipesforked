package behavioral.observer2;

/**
 * @author MaxAlex
 * @Date Created on 21 oct. 03
 *
 */

public class TestObserver {

	public static void main(String[] args) {
		// L'observé
		ForcesObservable forcesObservable = new ForcesObservable();
		
		// Les "Observateurs"
		ForcesXObserver forcesXObserver = new ForcesXObserver();
		ForcesYObserver forcesYObserver = new ForcesYObserver();
		
		forcesObservable.addObserver(forcesXObserver);
		forcesObservable.addObserver(forcesYObserver);

		
		System.out.println("forcesXObserver value = " 
			+ forcesXObserver.getValue());
		System.out.println("forcesYObserver value = " 
			+ forcesYObserver.getValue());
		
//		forcesObservable.setXValue(20);
		forcesObservable.setYValue(10);
		
		System.out.println("notifyObservers " );
		// Notify the Observers
		forcesObservable.notifyObservers();
//		forcesObservable.notifyObservers(forcesXObserver);
//		forcesObservable.notify();
//		forcesObservable.notifyAll();
//		forcesObservable.
		System.out.println("forcesXObserver value = " 
					+ forcesXObserver.getValue());
		System.out.println("forcesYObserver value = " 
					+ forcesYObserver.getValue());

	}
}
