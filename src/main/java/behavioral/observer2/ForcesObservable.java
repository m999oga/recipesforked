package behavioral.observer2;

import java.util.Observable;

/**
 * @author Olivier 
 * @Date Created on 21 oct. 03
 *
 */

/*
 * Observable

public void addObserver(Observer obs)

    Adds an observer to the internal list of observers.

public void deleteObserver(Observer obs)

    Deletes an observer from the internal list of observers.

public void deleteObservers()

    Deletes all observers from the internal list of observers.

public int countObservers()

    Returns the number of observers in the internal list of observers.

protected void setChanged()

    Sets the internal flag that indicates this observable has changed

    state.

protected void clearChanged()

    Clears the internal flag that indicates this observable has

    changed state.

public boolean hasChanged()

    Returns the boolean value true if this observable has changed

    state.

public void notifyObservers()

    Checks the internal flag to see if the observable has changed

    state and notifies all observers.

public void notifyObservers(Object obj)

    Checks the internal flag to see if the observable has changed

    state and notifies all observers. Passes the object specified in the

    parameter list to the notify() method of the observer.
 */
public class ForcesObservable extends Observable {

	private int forcesX=0;
	private int forcesY=0;
	private int forcesZ=0;
	
	public void setXValue(int forcesX){
		this.forcesX= forcesX;
		this.setChanged();
		System.out.println("X Value of ForcesObservable has Changed");
	}

	public int getXValue(){
		return this.forcesX;
	}
	
	public void setYValue(int forcesY){
		this.forcesY= forcesY;
		setChanged();
		System.out.println("Y Value of ForcesObservable has Changed");

	}

	public int getYValue(){
		return this.forcesY;
	}

	public void setZValue(int forcesZ){
		this.forcesZ= forcesZ;
		setChanged();
		System.out.println("Z Value of ForcesObservable has Changed");

	}

	public int getZValue(){
		return this.forcesZ;
	}
}
