package behavioral.observer2;

/**
 * @author MaxAlex
 * @Date Created on 21 oct. 03
 *
 */


public class ForcesXObserver extends ForcesObserver{

	
	protected int setValue(ForcesObservable forcesObservable){
		System.out.println("Refresh X Value of ForcesXObserver ");
		return forcesObservable.getXValue();
	}

}
