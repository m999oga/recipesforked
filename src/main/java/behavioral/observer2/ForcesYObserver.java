package behavioral.observer2;

/**
 * @author MaxAlex
 * @Date Created on 21 oct. 03
 *
 */


public class ForcesYObserver extends ForcesObserver{

	protected int setValue(ForcesObservable forcesObservable){
		System.out.println("Refresh Y Value of ForcesYObserver ");
		return forcesObservable.getYValue();
	}



}
