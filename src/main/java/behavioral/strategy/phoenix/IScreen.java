
package behavioral.strategy.phoenix;

/**
 * The IScreen interface defines the methods that need to be implemented by all the different
 * kind of screens.
 *
 *@author Gallez Olivier
 *@version 1.0
 *@serialData 
 */
public interface IScreen
{
    
    /**
     */
    public String loadScreen () throws Exception;
    
    /**
     */
    public String saveScreen () throws Exception;
    
	/**
	 */
    public String preLoadScreen() throws Exception;

	/**
	 */
    public String postLoadScreen() throws Exception;
    
}