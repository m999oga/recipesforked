package behavioral.strategy.phoenix;

import java.util.Hashtable;
import org.apache.log4j.Logger;

public class SLBContext  {
    private static Logger log =
        Logger.getLogger(SLBContext.class);
	

  private IScreen screen;
  private Hashtable properties;
  private WebScreens screens;
  	
  public SLBContext(String location)
  {
	screens = WebScreens.getInstance ();
    setScreen(location);
	if (log.isDebugEnabled()){
	  log.debug("SLBContext: Constructor entered");
	  log.debug("Screen: " + screen.getClass().getName());
	}
  }

  public void setScreen(String location)
  {
	this.screen= screens.getScreen (location);
  }

  public String loadScreen() throws Exception
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append(screen.preLoadScreen()+"\n");
    buffer.append(screen.loadScreen()+"\n");
    buffer.append(screen.postLoadScreen()+"\n");
    return buffer.toString();
  }

  public String saveScreen() throws Exception
  {
     if (log.isDebugEnabled()){
        log.debug("SLBContext: saveScreen called");
        log.debug("Screen: " + this.screen.getClass().getName());
     }
    return screen.saveScreen();
  }

}