package behavioral.strategy.phoenix;



import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;
import util.FileLoader;

public class WebScreens {
    private static Logger log =
        Logger.getLogger(WebScreens.class);

    private static String propertyFile 
    	= "behavioral/strategy/phoenix/resources/webscreens.properties";
    private static WebScreens instance = new WebScreens();
    private HashMap screens = new HashMap();

    private WebScreens() {
        this.loadProperties(propertyFile);
    }

    public static WebScreens getInstance() {
        return instance;
    }

    public IScreen getScreen(String location) {

        try {

          location = location.trim();
          Class aClass = getScreenClass( location);
          return (aClass != null) ? (IScreen) aClass.newInstance() : null;

        } catch (InstantiationException ex) {
            return null;
        } catch (IllegalAccessException ex) {
            return null;
        }
    }

    private Class getScreenClass( String location) {

        location = location.trim();
        Object obj = screens.get(location);
		return ( obj != null ) ? (Class) obj : null;
    }


    public void putScreenClass(String location, Class aScreen) {

        location = location.trim();
        screens.put(location, aScreen);
    }

    public void putScreenClass(String location, String className) {


        location = location.trim();
        className = className.trim();
        Class aScreen = null;
        try {

          aScreen = Class.forName(className);
          putScreenClass(location.trim(), aScreen);
        } catch (Exception ex) {

            log.error(" Class not found for " + className + " in location=" + location + ", " + ex);
        }
    }

    private void loadProperties(String url) {

        try {

            Properties properties = FileLoader.loadProperties(url);
            if (log.isDebugEnabled()){
                log.debug("WebScreens loading properties :" + properties);
            }
            Enumeration<?> enurt = properties.propertyNames();
            int i = 0;
            String location;
            while (enurt.hasMoreElements()) {

                location = (String) enurt.nextElement();
                putScreenClass(location, properties.getProperty(location));
                i++;
            }
            if (i == 0) {

                if (log.isDebugEnabled()){
                  log.debug(" No properties were found for webScreens: properties=" + properties);
                }
            }
        } catch (Exception ex) {

          log.error("Properties not found, name = " + url);
        }
    }
}
