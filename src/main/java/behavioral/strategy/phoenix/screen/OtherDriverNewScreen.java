package behavioral.strategy.phoenix.screen;

import behavioral.strategy.phoenix.IScreen;

/**
 * @author H2R
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class OtherDriverNewScreen implements IScreen {

	/**
	 */
	public String loadScreen() throws Exception {
		return new String("OtherDriverNewScreen : loadScreen");
	}

	/**
	 */
	public String saveScreen() throws Exception {
		return new String("OtherDriverNewScreen : saveScreen");
	}

	/**
	 */
	public String preLoadScreen() throws Exception {
		return new String("OtherDriverNewScreen : preLoadScreen");
	}

	/**
	 */
	public String postLoadScreen() throws Exception {
		return new String("OtherDriverNewScreen : postLoadScreen");
	}


}
