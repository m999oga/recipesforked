/*
 * Created on 30-d�c.-03
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package behavioral.strategy.phoenix.screen;

import behavioral.strategy.phoenix.IScreen;

/**
 * @author H2R
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PolicyholderNewScreen implements IScreen {


	/**
	 */
	public String loadScreen() throws Exception {
		return new String("PolicyholderNewScreen : loadScreen");
	}

	/**
	 */
	public String saveScreen() throws Exception {
		return new String("PolicyholderNewScreen : saveScreen");
	}

	/**
	 */
	public String preLoadScreen() throws Exception {
		return new String("PolicyholderNewScreen : preLoadScreen");
	}

	/**
	 */
	public String postLoadScreen() throws Exception {
		return new String("PolicyholderNewScreen : postLoadScreen");
	}

}
