package behavioral.strategy.phoenix.screen;

import behavioral.strategy.phoenix.IScreen;

/**
 * @author H2R
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MainDriverNewScreen implements IScreen {


	/**
	 */
	public String loadScreen() throws Exception {
		return new String("MainDriverNewScreen : loadScreen");
	}

	/**
	 */
	public String saveScreen() throws Exception {
		return new String("MainDriverNewScreen : saveScreen");
	}

	/**
	 */
	public String preLoadScreen() throws Exception {
		return new String("MainDriverNewScreen : preLoadScreen");
	}

	/**
	 */
	public String postLoadScreen() throws Exception {
		// TODO Auto-generated method stub
		return new String("MainDriverNewScreen : postLoadScreen");
	}


}
