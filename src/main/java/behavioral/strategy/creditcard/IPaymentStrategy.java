package behavioral.strategy.creditcard;

public interface IPaymentStrategy {
    public void pay(int amount);
}
