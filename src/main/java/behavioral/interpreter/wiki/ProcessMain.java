package behavioral.interpreter.wiki;

public class ProcessMain {
    public static void main(String[] args) {
        String expression = "42 4 2 - +";
        Parser p = new Parser(expression);
        System.out.println("'" + expression +"' equals " + p.evaluate());
    }
}

/* Display 
 * 
 * '42 4 2 - +' equals 44
 *  
 */
 