package util;
import java.io.* ;
import java.net.URL ;
import java.util.* ;



/**
 * Utility to load property files and to get an open Inputstream on whatever file
 *
 * @author Herman Van Durme
 * @version 1.0
 * @since 22/08/2001
 */
public class FileLoader
{

    /**
     *
     * @param resourcePathRelativeToClasspath a string containing the file name, should be something like : "resource/path/relative/to/classpath/filename"
     * @return an open InputStream on the resource in the classpath,
     * @exception java.io.IOException When file does not exist in classpath
     */
    public static InputStream getInputStreamFor(String resourcePathRelativeToClasspath ) throws java.io.IOException
    {
        FileLoader fl = new FileLoader() ;
        return fl.getClass().getClassLoader().getResourceAsStream(resourcePathRelativeToClasspath);
        //prop.load(fl.getClass().getResourceAsStream(resourcePathRelativeToClasspath));
        //return new FileLoader().getInputStreamForInstance(resourcePathRelativeToClasspath);
    }

    public InputStream getInputStreamForInstance (String resourcePathRelativeToClassPath) throws java.io.IOException  {

      ClassLoader cl = this.getClass().getClassLoader();

      URL url = cl.getResource(resourcePathRelativeToClassPath);
      if (url == null)
      {
          throw new java.io.IOException("File <" + resourcePathRelativeToClassPath + "> not found" ) ;
      }

      InputStream rf = url.openStream();
      return rf ;

    }


    //  ************* VERY IMPORTANT *************
    //
    //  When specifying resourcePathRelativeToClasspath, use
    //  "//" as your separators and not ".".
    //  eg. be//axa//richards//large//package is GOOD
    //  be.axa.simons.small.package is NOT GOOD
    //
    //  ******************************************
    //
    public static File getFile(String resourcePathRelativeToClasspath)
       throws FileNotFoundException {

       ClassLoader cl = null;
       URL url = null;
       File result = null;

       cl = FileLoader.class.getClassLoader();

/* FUTURE
        if (Log.isDebugEnabled()){
            Log.debug("resourcePathRelativeToClasspath = " +resourcePathRelativeToClasspath);
        }
*/        
       url = cl.getResource(resourcePathRelativeToClasspath);


       try {
            result = new File(url.getFile());
       }
       catch(NullPointerException ex) {
            throw new FileNotFoundException("Could not find " + resourcePathRelativeToClasspath);
       }
       return result;
    }

    public static File getFile(String packageName, String fileName)
       throws FileNotFoundException {

       String resourcePath = null;
       ClassLoader cl = null;
       URL url = null;
       File result = null;

       cl = FileLoader.class.getClassLoader();
       resourcePath = packageName.replace('.',File.separatorChar) + File.separatorChar + fileName;
       url = cl.getResource(resourcePath);

       try {
            result = new File(url.getFile());
       }
       catch(NullPointerException ex) {
            throw new FileNotFoundException("Could not find " + resourcePath);
       }
       return result;
    }

    public static PropertyResourceBundle getPropertyResourceBundle(
       String packageName,
       String fileName)
       throws IOException {

       String resourcePath = null;
       ClassLoader cl = null;
       URL url = null;
       PropertyResourceBundle result = null;

       cl = FileLoader.class.getClassLoader();
       resourcePath = packageName.replace('.',File.separatorChar) + File.separatorChar + fileName;
       url = cl.getResource(resourcePath);

       try {
            result = new PropertyResourceBundle(url.openStream());
       }
       catch(NullPointerException ex) {
            throw new FileNotFoundException("Could not find " + resourcePath);
       }
       return result;
    }

    /**
     * Loads a properties file
     * Update for iPlanet, apparently the class side load properties doesn't work.
     * Some made the change that th class side now uses an instance to load a properties file.
     *
     * @param resourcePathRelativeToClasspath a string containing the file name, should be something like : "resource/path/relative/to/classpath/filename"
     * @return an instance of Properties
     * @exception java.io.IOException When file cannot be opened o incorrect properties file
     */

    public static Properties loadProperties(String resourcePathRelativeToClasspath) throws java.io.IOException
    {

       FileLoader fl = new FileLoader() ;


       if ( resourcePathRelativeToClasspath.charAt(0) != '/' )
       {
         return fl.loadPropertiesInstance( "/" + resourcePathRelativeToClasspath ) ;
       }
       else
       {
         return fl.loadPropertiesInstance( resourcePathRelativeToClasspath ) ;
       }
    }

	public Properties loadPropertiesInstance(String resourcePathRelativeToClasspath) throws java.io.IOException
	{
            FileLoader fl = new FileLoader() ;
            Properties prop = new Properties();
            prop.load(fl.getClass().getResourceAsStream(resourcePathRelativeToClasspath));
            return prop ;
	}

    public static boolean fileExists (String resourcePackage, String fileName)
    {
        File file;
        try
        {
            file = getFile(resourcePackage, fileName);
            return true;
        }
        catch (FileNotFoundException fnfEx)
        {
            return false;
        }
        finally
        {
            file = null;
        }

    }
}