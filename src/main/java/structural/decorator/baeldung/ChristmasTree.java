package structural.decorator.baeldung;

public interface ChristmasTree {
    String decorate();
}
