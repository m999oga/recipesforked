package log;

import org.apache.log4j.Category;
import org.apache.log4j.PropertyConfigurator;

//import util.FileLoader;


import util.FileLoader;

import java.io.FileInputStream;
import java.util.Properties;

/**
 <br>How to use the Log class:<br>

 <br>This logging should be used in place of System.out.println statements.
 <br>For classes where you want to implement logging, import org.apache.log4j.Category<br>
 You will need to have the log4j.jar in your classpath<br>
 Change the name of config file in the Log class to your config filename. <br>
 <br>Place the following as a class variable:
 <code>static Category log = Category.getInstance(Log.class.getName());</code> where "Log" is your class name
 <br>At least once, at the beginning of your testing, instantiate an instance of the Log class.
 e.g. <code>Log logger = new Log();</code><p>
 You do NOT need to instantiate a new instance of log for each class, but only once at the beginning
 of your test!
 <br>Place the properties file where it can be found (normally where the .java files are found)
 <br>You can modify the properties file to suit your needs.  You can set it in many ways, to either
 write to console, write to a file, or both.
 <br>You can also set exactly what gets output.  A partial list follows:<br>
 %d    = date <br>
 %-4r  = number of milliseconds since program start<br>
 []    = put inside []<br>
 %t    = calling method name<br>
 %n    = /new line/<br>
 3	= an integer indicates the offset, ie the number of spaces available for this section of the logging.<br>
 %p	= priority. <br>
 <i>If you set a different priority (ie ERROR) only that priority <b>AND PRIORITIES BELOW IT IN THE HIERARCHY </b>	will be logged.<br>  The hierarchy is: <b>DEBUG &lt; INFO &lt; WARN &lt; ERROR &lt; FATAL</b></i><br>
 %f	= class name<br>
 %l 	= line number in class<br>
 %m 	= message<br>
 <p>
 Consult the manual at www.apache.org for everything log4j can do.  It is
 VERY cool.
 </p>

 */

public class Log {

    static Category log = Category.getInstance(Log.class.getName());

    /**
     * Read the Log4J properties from the resources directory and configure the Log4J instance.
     */
    static {
        Properties properties = new Properties();
        try {
			properties = FileLoader.loadProperties("log/LogConfig.properties");
/*
			//properties = FileLoader.getProperties("D:\\DVLP\\JavaBase\\log\\LogConfig.properties");
			String resourcePathToClasspath = "D:\\bea\\wlserver6.1\\lib\\Log4J\\LogConfig.properties";
			FileInputStream file = new FileInputStream(resourcePathToClasspath); 
			properties.load(file);
*/
        } catch (Exception ex) {				   
            System.out.println(ex);
        }
        PropertyConfigurator.configure(properties);
    }

    /**
     * Logs message whit fatal priority tag to the logfile (by delegation)
     * @param msg the message to be written to the log
     */
    public static void fatal(Object msg) {
        log.fatal(msg);
    }
    /**
     * Logs message whit fatal priority tag to the logfile (by delegation)
     * @param msg the message to be written to the log
     * @param t the exception that has occured
     */
    public static void fatal(Object msg, Throwable t) {
        log.fatal(msg, t);
    }

    /**
     * Logs error message(by delegation) to the logfile
     * @param msg the message to be written to the log
     */
    public static void error(Object msg) {
        log.error(msg);
    }
    /**
     * Logs error message to the logfile (by delegation)
     * @param msg the message to be written to the log
     * @param t the exception that has occured
     */
    public static void error(Object msg, Throwable t) {
        log.error(msg, t);
    }

    /**
     * Logs warning message to the logfile (by delegation)
     * @param msg the message to be written to the log
     */
    public static void warn(Object msg) {
        log.warn(msg);
    }
    /**
     * Logs warning message to the logfile (by delegation)
     * @param msg the message to be written to the log
     * @param t the exception that has occured
     */
    public static void warn(Object msg, Throwable t) {
        log.warn(msg, t);
    }

    /**
     * Logs information to the logfile (by delegation)
     * @param msg the message to be written to the log
     */
    public static void info(Object msg) {
        log.info(msg);
    }
    /**
     * Logs information to the logfile (by delegation)
     * @param msg the message to be written to the log
     * @param t the exception that has occured
     */

    public static void info(Object msg, Throwable t) {
        log.info(msg, t);
    }

    /**
     * Logs debug information to the logfile (by delegation)
     * @param msg the message to be written to the log
     */
    public static void debug(Object msg) {
        log.debug(msg);
    }

    /**
     * Logs debug information (by delegation) to the logfile
     * @param msg the message to be written to the log
     * @param t the exception that has occured
     */
    public static void debug(Object msg, Throwable t) {
        log.debug(msg, t);
    }

    /**
     * Returns true if the priority level is set to DEBUG (by delegation)
     * @return true - if the priority level is DEBUG
     */
    public static boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }

    /**
     * Logs debug information that will show which method of which class is executing.
     * @param className The name of the class that is executing
     * @param methodName The name of the method that is executing
     */
    public static void debugMethod(String className, String methodName) {
        if (Log.isDebugEnabled()) {
            /* Don't delegate to Log.debugMethodSignature (className, methodName + "()");
             * to improve performance.
             */
            Log.debug("********** Executing method **********");
            Log.debug(className + "#" + methodName + "()");
            Log.debug("********** Executing method **********");
        }
    }

    /**
     * Logs debug information that will show which method of which class is executing.
     * @param className The name of the class that is executing
     * @param methodSignature The signature (including parameters, not including return result and exceptions)
     *      of the method that is executing.
     */
    public static void debugMethodSignature(String className, String methodSignature) {
        if (Log.isDebugEnabled()) {
            Log.debug("********** Executing method **********");
            Log.debug(className + "#" + methodSignature);
            Log.debug("********** Executing method **********");
        }
    }

    /**
     * Logs debug information that shows which method of which object is executing
     * @param receiver The object that is executing the method
     * @param methodName The name of the method that is executing
     */
    public static void debugMethod(Object receiver, String methodName) {
        if (Log.isDebugEnabled()) {
            /* Don't delegate to Log.debugMethod (receiver.getClass ().getName (), methodName);
             * to improve performance.
             */
            Log.debug("********** Executing method **********");
            Log.debug(receiver.getClass().getName() + "#" + methodName + "()");
            Log.debug("********** Executing method **********");
        }
    }

    /**
     * Logs debug information that will show which method of which class is executing.
     * @param receiver The object that is executing the method
     * @param methodSignature The signature (including parameters, not including return result and exceptions)
     *      of the method that is executing.
     */
    public static void debugMethodSignature(Object receiver, String methodSignature) {
        if (Log.isDebugEnabled()) {
            /* Don't delegate to Log.debugMethodSignature (receiver.getClass ().getName (), methodSignature);
             * to improve performance.
             */
            Log.debug("********** Executing method **********");
            Log.debug(receiver.getClass().getName() + "#" + methodSignature);
            Log.debug("********** Executing method **********");
        }
    }

    public static void stackTrace() {
        Log.error(new Exception("For debugging purposes only!"));
    }

    public static void stackTrace(String message) {
        Log.error(new Exception("For debugging purposes only - " + message));
    }

    /**
     * Logs message at DEBUG level
     * @param msg
     * @see Log#debug(Object msg)
     */
    public static void log(String msg) {
        debug(msg);
    }

    /**
     * Writes error message to the log.
     * @param e The error that has be thrown, and has to be written to the logfile
     */
    public static void error(Exception e) {
        error(e.getMessage(), e);
        //e.printStackTrace();
    }
}