package log;

import org.apache.log4j.Category;

public class TestLogAlone {

    /**
     * 1) Place the following as a class variable where "Log" is your class name
     */

    static Category Log = Category.getInstance(Log.class.getName());

    public static void main(String[] args) {
	/**
	 * 	At least once, at the beginning of your testing, 
	 * 	instantiate an instance of the Log class.
	 *  
	 */
	Log logger = new Log();
	
	
	/**
	 *  You do NOT need to instantiate a new instance of log for each class,
	 *  but only once at the beginning of your test!
	 *  
	 *  Place the properties file where it can be found (normally where 
	 *  the .java files are found) You can modify the properties file 
	 *  to suit your needs.  
	 *  
	 *  You can set it in many ways, to either write to console, 
	 *  write to a file, or both.
	 */
	logger.debug("My First message");
	/*
	 * You can also set exactly what gets output.  A partial list follows:
	 * 	%d    = date 
	 * 	%-4r  = number of milliseconds since program start
	 * []    = put inside []
	 * %t    = calling method name
	 * %n    = /new line/
	 * 3	= 	an integer indicates the offset, ie the number of spaces
	 * 		available for this section of the logging.
	 * %p	= priority. The hierarchy is: DEBUG  INFO  WARN  ERROR  FATAL
	 * %f	= class name
	 * %l 	= line number in class
	 * %m 	= message
	 */


	
	
	
    }

}
